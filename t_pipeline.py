import t_mask
import t_math
import t_config
import os
import numpy as np
import copy
from scipy.stats import norm
import glob
from sunpy.net import Fido, attrs as a
from astropy import units as u

class t_pipeline(object):
   

    def download_files(self, begin_time, end_time, wavelength, cadence, files_path):
        

        print('Making request...\n') 
        #search_result = Fido.search(a.Time(begin_time, end_time), a.Instrument(instrument), a.Wavelength(wavelength*u.angstrom),a.vso.Sample(12*u.second))
        search_result = Fido.search(a.Time(begin_time, end_time), a.Instrument.aia, a.Wavelength(wavelength*u.angstrom),a.vso.Sample(12*u.second))
        download_files=Fido.fetch(search_result,path=files_path+'/download/{file}.fits')
        #download_files=Fido.fetch(search_result,files_path+'/download/{file}.fits')
        
        
        
    def base_diff_series(self, path, preproc_config ):
    #create a base image out of the series 

        files_arr = os.listdir(path)
        files_arr.sort()
        
        fname=path+files_arr[0]
        mask_base = t_mask.t_mask()  
        mask_base.preproc_config=preproc_config

        mask_base.read_input_file(fname)
        mask_base_arr = np.array(mask_base.img_init_data) 
        
        print('Calculating base difference image...')

        for file_i in range(1,len(files_arr)):
                                    
            fname=path+files_arr[file_i]
            print('Processing file:'+fname)
            
            mask_i = t_mask.t_mask() 
            mask_i.preproc_config=preproc_config
            #mask_i.decompose_config=decomposse_config
            #mask_i.circle_mask=self.circle_mask
            #mask_i.square_mask=self.square_mask

            mask_i.timestep_i=file_i
            mask_i.read_input_file(fname)
            
            mask_i_arr = np.array(mask_i.img_init_data)
           
            mask_base_arr=np.add(mask_base_arr, mask_i_arr)
            
            del mask_i 
        

        mask_base_arr=np.divide(mask_base_arr, len(files_arr))
        mask_base.img_init_data=np.array(mask_base_arr)
        return mask_base
    

    def find_objects_mla_uni(self, mask_obj,prev_mask_obj, mask_base_obj):
      
        a_mask = t_mask.t_mask()
        
        a_mask.preproc_config=copy.deepcopy(mask_obj.preproc_config)
        a_mask.decompose_config=copy.deepcopy(mask_obj.decompose_config) 
        a_mask.circle_mask= mask_obj.circle_mask #self.circle_mask
        a_mask.square_mask= mask_obj.square_mask
        a_mask.timestamp = mask_obj.timestamp
        a_mask.datetime_str = mask_obj.datetime_str
        a_mask.wavelength = mask_obj.wavelength
        a_mask.timestep_i =  mask_obj.timestep_i   
        a_mask.img_init_data_orig = list(mask_obj.img_init_data_orig)


        a_math = t_math.t_math()
    
        img_init_data_arr = np.array(mask_obj.img_init_data)
        img_init_data_arr_prev = np.array(prev_mask_obj.img_init_data) 
        
        if (a_mask.preproc_config.use_base_diff=='y'): 
                  
            print('Calculating base difference image...')
            base_img_init_data_arr = np.array(mask_base_obj.img_init_data) 
            img_init_data_arr=np.subtract(img_init_data_arr, base_img_init_data_arr)           

        if (a_mask.preproc_config.diff_algorithm=='AWARE'):
            print('Performing AWARE algorithm on running differences ')   
            img_init_data_arr = (img_init_data_arr>img_init_data_arr_prev)*img_init_data_arr 
            
        elif (a_mask.preproc_config.diff_algorithm=='RD'):
            print('Calculating Running Difference image')
            img_init_data_arr = np.subtract(img_init_data_arr,img_init_data_arr_prev)#*img_init_data_arr 
    
        img_init_low_trunc = (img_init_data_arr>mask_obj.preproc_config.low_threshold_bd)*img_init_data_arr
        img_init_trunc = (img_init_low_trunc<mask_obj.preproc_config.high_threshold_bd)*img_init_low_trunc
        img_init_trunc_32=a_math.bring_to_int32(img_init_trunc)
        
        if (a_mask.preproc_config.data_invert_pipeline=='y'):
         
            print('Inverting the data')

            if (a_mask.preproc_config.data_invert_method=='numeric'):
                img_init_trunc_32=a_math.invert_data_numeric(img_init_trunc_32)
            elif (a_mask.preproc_config.data_invert_method=='img'):
                img_init_trunc_32=a_math.invert_data_img(img_init_trunc_32) 
          
        
        if (mask_obj.preproc_config.normalize_init_data=='y'): 
           img_init_trunc_32_norm = a_math.normalize_posterize(img_init_trunc_32, mask_obj.decompose_config.resolution_bits)
           a_mask.img_init_data=list(img_init_trunc_32_norm)

        else: 
           a_mask.img_init_data=list(img_init_trunc_32)
         

        a_mask.calc_scales() 
        a_mask.scales_post_process()

        if (mask_obj.decompose_config.verbose_output=='y'):
            a_mask.save_scales(os.getcwd()+mask_obj.decompose_config.log_save_path) 
         

        work_mask = t_mask.t_mask()

        work_mask.preproc_config=copy.deepcopy(mask_obj.preproc_config)
        work_mask.decompose_config=copy.deepcopy(mask_obj.decompose_config)
        work_mask.header_orig=copy.deepcopy(mask_obj.header_orig)
        work_mask.circle_mask=mask_obj.circle_mask
        work_mask.square_mask=mask_obj.square_mask
        work_mask.timestamp = mask_obj.timestamp
        work_mask.datetime_str = mask_obj.datetime_str
        work_mask.wavelength = mask_obj.wavelength
        work_mask.timestep_i =  mask_obj.timestep_i 

        work_mask.decompose_config.scales_n_max=3
      
        work_mask.img_init_data=list(a_mask.img_init_data)
        work_mask.img_init_data_orig = list(a_mask.img_init_data_orig)
        work_mask.reference_mask = list(a_mask.reference_mask)
    
        work_mask.w_coeffs.append(a_mask.w_coeffs[0])
        work_mask.w_coeffs.append(a_mask.w_coeffs[a_mask.decompose_config.scales_n_max-2])
        work_mask.w_coeffs.append(a_mask.w_coeffs[a_mask.decompose_config.scales_n_max-1])
        
        work_mask.scales.append(a_mask.scales[0])
        work_mask.scales.append(a_mask.scales[a_mask.decompose_config.scales_n_max-2])
        work_mask.scales.append(a_mask.scales[a_mask.decompose_config.scales_n_max-1])

        work_mask.scales_post_process()
        
        bkg_mask = t_mask.t_mask()        
        bkg_mask.preproc_config=copy.deepcopy(mask_obj.preproc_config)
        bkg_mask.decompose_config=copy.deepcopy(mask_obj.decompose_config)
        
        bkg_mask.circle_mask=mask_obj.circle_mask
        bkg_mask.square_mask=mask_obj.square_mask
        bkg_mask.timestamp = mask_obj.timestamp
        bkg_mask.datetime_str = mask_obj.datetime_str
        bkg_mask.wavelength = mask_obj.wavelength
        bkg_mask.timestep_i =  mask_obj.timestep_i 
          
        bkg_mask.img_init_data= list(a_mask.img_init_data) # list(a_mask.img_init_data_orig)
        bkg_mask.img_init_data_orig = list(a_mask.img_init_data_orig)
        bkg_mask.reference_mask = list(a_mask.reference_mask)

        if (mask_obj.preproc_config.data_invert_bkg == 'y'): 
               
            print('Inverting the background data')

            if (a_mask.preproc_config.data_invert_method_bkg=='numeric'):
                bkg_mask.img_init_data=a_math.invert_data_numeric(bkg_mask.img_init_data)
            elif (a_mask.preproc_config.data_invert_method_bkg=='img'):
                bkg_mask.img_init_data=a_math.invert_data_img(np.array(bkg_mask.img_init_data)) 
       

        bkg_mask.decompose_config.resolution_bits = mask_obj.decompose_config.bkg_resolution_bits
        bkg_mask.decompose_config.thrhold_sigma= mask_obj.decompose_config.bkg_thrhold_sigma
        bkg_mask.decompose_config.grad_thrhold_sigma= mask_obj.decompose_config.bkg_grad_thrhold_sigma
        bkg_mask.decompose_config.outside_thrhold_sigma=mask_obj.decompose_config.bkg_outside_thrhold_sigma
        bkg_mask.decompose_config.scales_weights=copy.deepcopy(mask_obj.decompose_config.bkg_scales_weights)
    
       # bkg_mask.decompose_config.normalize='y'
      
        bkg_mask.circle_mask= copy.deepcopy(mask_obj.circle_mask)
        
        work_mask.square_mask=copy.deepcopy(mask_obj.square_mask)

        bkg_mask.timestamp = mask_obj.timestamp
        bkg_mask.datetime_str = mask_obj.datetime_str
        bkg_mask.wavelength = mask_obj.wavelength
        bkg_mask.timestep_i =  mask_obj.timestep_i 
       
        bkg_mask.header_orig=copy.deepcopy(mask_obj.header_orig)
        bkg_mask.header_orig['DATAMIN']=np.amin(bkg_mask.img_init_data) 
        bkg_mask.header_orig['DATAMAX']=np.amax(bkg_mask.img_init_data)

        print('Preparing background image...')
        bkg_mask.calc_scales()
        bkg_mask.scales_post_process()
        
        if (mask_obj.decompose_config.override_w_bkg=='y'): 
            work_mask = copy.deepcopy(bkg_mask)
            work_mask.w_coeffs.append(bkg_mask.w_coeffs[0])
            work_mask.w_coeffs.append(bkg_mask.w_coeffs[a_mask.decompose_config.scales_n_max-2])
            work_mask.w_coeffs.append(bkg_mask.w_coeffs[a_mask.decompose_config.scales_n_max-1])
        
            work_mask.scales.append(bkg_mask.scales[0])
            work_mask.scales.append(bkg_mask.scales[a_mask.decompose_config.scales_n_max-2])
            work_mask.scales.append(bkg_mask.scales[a_mask.decompose_config.scales_n_max-1])
        
        a_mask.img_bkg_data=list(bkg_mask.decompositions[bkg_mask.decompose_config.scales_n_max-2])  
        
        if (work_mask.decompose_config.verbose_output=='y'):
            work_mask.save_decompositions(os.getcwd()+work_mask.decompose_config.log_save_path)


        if work_mask.decompose_config.grad_method=='Simple_grad': 
            work_mask.decompositions_simple_grad()

        if work_mask.decompose_config.grad_method=='Sobel-Feldman':       
            work_mask.decompositions_sobel_feldman()
        
        if work_mask.decompose_config.grad_method=='Hagenaer': 
            work_mask.decompositions_hagenaer()

        work_mask.apply_reference_mask_grads()

        work_mask.save_decomposition_grads(os.getcwd()+work_mask.decompose_config.grad_save_path) 
           
        work_mask.structures_masks(os.getcwd()+work_mask.decompose_config.struct_save_path,work_mask.decompositions_raw[2],work_mask.decompositions[2],work_mask.timestep_i,0,work_mask.decompose_config.min_struct_size)
     
        work_mask.apply_combined_structs_mask()  

        print('Putting strucures and gradients as layers above the initial data')   
        #init_img_data_orig_int_32= a_math.bring_to_int32(np.array(work_mask.img_init_data_orig))
        img_data_bkg_int_32= a_math.bring_to_int32(np.array(bkg_mask.decompositions[bkg_mask.decompose_config.scales_n_max-2]))
        
        img_init_data_orig_int_32 = list(work_mask.img_init_data_orig)
        
        #AWARE/Running diff/base_diff depending on the configuration 
        img_init_data_int_32 = list(work_mask.img_init_data)
        
        #for training sets: 
        img_init_data_orig_w_structs_arr=work_mask.bind_2_layers_region(img_init_data_orig_int_32,work_mask.decompositions_raw[2])
        img_init_data_orig_w_grads_arr= work_mask.bind_2_layers_region(img_init_data_orig_int_32,work_mask.decompositions_grads_ref[2])
       
        #for visualizations: 
        bkg_data_w_structs_arr=work_mask.bind_2_layers_XOR(img_data_bkg_int_32,work_mask.decompositions_raw[2])
        bkg_data_w_grads_arr= work_mask.bind_2_layers_XOR(img_data_bkg_int_32,work_mask.decompositions_grads_ref[2])

        #for training sets, AWARE/RD/BASE_DIFF: 
        img_init_data_w_structs_arr=work_mask.bind_2_layers_XOR(img_init_data_int_32,work_mask.decompositions_raw[2])
        img_init_data_w_grads_arr= work_mask.bind_2_layers_XOR(img_init_data_int_32,work_mask.decompositions_grads_ref[2])
        
        #work_mask.orig_data_w_structs = list(orig_data_w_structs_arr)
        #work_mask.orig_data_w_grads = list(orig_data_w_grads_arr) 
        
        diff_masks_fname_part='masks_projected_'+work_mask.preproc_config.diff_algorithm+'_data'
        diff_masks_fname_part_grad='masks_grads_projected_'+work_mask.preproc_config.diff_algorithm+'_data'
        diff_masks_fname_part_appl='masks_applied_'+work_mask.preproc_config.diff_algorithm+'_data'
        diff_fname_part=work_mask.preproc_config.diff_algorithm+'_data'

        
        use_frame=1
    
        if ( mask_obj.preproc_config.data_lev.find('AIA')>=0):
            if (mask_obj.header_orig['AECTYPE']!=0): 
                use_frame=0

        if (use_frame==1):

            combined_masks_arr=np.array(work_mask.structs_combined_mask)
            combined_masks_raw_arr=np.array(work_mask.structs_combined_raw)
            grads_masks_arr=np.array(work_mask.decompositions_grads_ref[2])
            

            fname_bkg_bw=work_mask.save_data(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'bkg_bw', np.array(img_data_bkg_int_32), work_mask.timestep_i,0, 'n')       
           
            fname_masks_raw=work_mask.save_data(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'masks_raw', combined_masks_raw_arr, work_mask.timestep_i,0, 'y') 
            fname_masks=work_mask.save_data(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'masks', combined_masks_arr, work_mask.timestep_i,0, 'y')
            fname_masks_grads=work_mask.save_data(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'masks_grads', grads_masks_arr, work_mask.timestep_i,0, 'y')
             
            raw_plus_grads_masks_arr=work_mask.bind_2_layers_XOR(combined_masks_raw_arr, grads_masks_arr)
            
            fname_raw_w_grads=work_mask.save_data(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'masks_raw_w_grads', raw_plus_grads_masks_arr, work_mask.timestep_i,0, 'y')
            #fname_raw_w_grads_2=work_mask.save_data(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'masks_raw_w_grads_transp', raw_plus_grads_masks_arr, work_mask.timestep_i,0, 'y')

            fname_masks_grads_bkg_bw_projected=fname_bkg_bw.replace("bkg_bw", "masks_raw_w_grads_projected_bkg_2", 1)      
            fname_masks_grads_bkg_bw_projected_2=fname_bkg_bw.replace("bkg_bw", "masks_raw_w_grads_projected_bkg", 1) 
            fname_masks_raw_rnb_bkg_bw_projected=fname_bkg_bw.replace("bkg_bw", "masks_raw_rnb_projected_bkg", 1) 
            a_math.merge_two_layers_transp_r_save_png( np.array(img_data_bkg_int_32), raw_plus_grads_masks_arr, fname_masks_grads_bkg_bw_projected) 
            

            work_mask.save_data(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'masks_projected_bkg_bw', bkg_data_w_structs_arr, work_mask.timestep_i,0, 'n')
            work_mask.save_data(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'masks_grads_projected_bkg_bw', bkg_data_w_grads_arr, work_mask.timestep_i,0, 'n')
       
            work_mask.save_data(os.getcwd()+work_mask.decompose_config.visualize_save_path, diff_masks_fname_part, img_init_data_w_structs_arr, work_mask.timestep_i,0, 'y')
            work_mask.save_data(os.getcwd()+work_mask.decompose_config.visualize_save_path, diff_masks_fname_part_grad, img_init_data_w_grads_arr, work_mask.timestep_i,0, 'y')
            work_mask.save_data(os.getcwd()+work_mask.decompose_config.visualize_save_path, diff_masks_fname_part+'_bw', img_init_data_w_structs_arr, work_mask.timestep_i,0, 'n')
            work_mask.save_data(os.getcwd()+work_mask.decompose_config.visualize_save_path, diff_masks_fname_part_grad+'_bw', img_init_data_w_grads_arr, work_mask.timestep_i,0, 'n')
            
        
            fname_masks_raw_fits=work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'masks_raw_data', np.array(work_mask.structs_combined_raw), work_mask.timestep_i,0, mask_obj.header_orig)
                     
            if (work_mask.decompose_config.override_bkg_w_orig=='n'):
                fname_bkg_fits=work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'bkg', np.array(img_data_bkg_int_32), work_mask.timestep_i,0, bkg_mask.header_orig)
            else: 
                fname_bkg_fits=work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'bkg', np.array(img_init_data_orig_int_32), work_mask.timestep_i,0, bkg_mask.header_orig)
           

            fname_masks_raw_rnb_bkg_bw_projected=fname_bkg_fits.replace("bkg", "masks_raw_rnb_projected_bkg", 1) 
            fname_masks_raw_rnb_bkg_bw_projected=fname_masks_raw_rnb_bkg_bw_projected.replace("fits", "png", 1) 
           

            a_math.merge_two_fits(fname_bkg_fits,fname_masks_raw_fits, fname_masks_raw_rnb_bkg_bw_projected)
            
            work_mask.header_orig['DATAMIN']=np.amin(work_mask.img_init_data) 
            work_mask.header_orig['DATAMAX']=np.amax(work_mask.img_init_data)

            work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'masks', np.array(work_mask.structs_combined_mask), work_mask.timestep_i,0, work_mask.header_orig)
            work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'masks_grads', np.array(grads_masks_arr), work_mask.timestep_i,0, work_mask.header_orig)
        

            work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.visualize_save_path, diff_masks_fname_part, img_init_data_w_structs_arr, work_mask.timestep_i,0, work_mask.header_orig)
            work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.visualize_save_path, diff_masks_fname_part_grad, img_init_data_w_grads_arr, work_mask.timestep_i,0, work_mask.header_orig)

            work_mask.save_data(os.getcwd()+work_mask.decompose_config.trainset_save_path, 'masks_applied_orig_data', img_init_data_orig_w_structs_arr, work_mask.timestep_i,0, 'y')
            work_mask.save_data(os.getcwd()+work_mask.decompose_config.trainset_save_path, 'masks', np.array(work_mask.structs_combined_mask), work_mask.timestep_i,0, 'n')
            work_mask.save_data(os.getcwd()+work_mask.decompose_config.trainset_save_path, diff_masks_fname_part_appl, np.array(work_mask.structs_combined_raw), work_mask.timestep_i,0, 'n')
            work_mask.save_data(os.getcwd()+work_mask.decompose_config.trainset_save_path, 'orig_data', img_init_data_orig_int_32, work_mask.timestep_i,0, 'n')
            work_mask.save_data(os.getcwd()+work_mask.decompose_config.trainset_save_path, diff_fname_part,img_init_data_int_32,work_mask.timestep_i,0, 'n')

            work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.trainset_save_path, 'masks_applied_orig_data', img_init_data_orig_w_structs_arr, work_mask.timestep_i,0, work_mask.header_orig)
            work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.trainset_save_path, 'masks', np.array(work_mask.structs_combined_mask), work_mask.timestep_i,0, work_mask.header_orig)
            work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.trainset_save_path, diff_masks_fname_part_appl, np.array(work_mask.structs_combined_raw), work_mask.timestep_i,0, work_mask.header_orig)
            work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.trainset_save_path, 'orig_data', img_init_data_orig_int_32, work_mask.timestep_i,0, work_mask.header_orig)
            work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.trainset_save_path, diff_fname_part,img_init_data_int_32,work_mask.timestep_i,0, work_mask.header_orig)
            

        return copy.deepcopy(work_mask)


    def track_objects_mla_uni(self, mask_base, preproc_config, decompose_config):

        a_math=t_math.t_math()
        files_arr = os.listdir(os.getcwd()+preproc_config.data_path)
        files_arr.sort()
     
        results_coord_f = open(os.getcwd()+decompose_config.results_coord_fname, 'w')
        results_overlap_f = open(os.getcwd()+decompose_config.results_overlap_fname, 'w')
        overlap_count = 0
        


        for file_i in range(1,len(files_arr)):
                                    
            fname=os.getcwd()+preproc_config.data_path+files_arr[file_i]
            print('Processing file:'+fname)
                   
            mask_i = t_mask.t_mask() 
            mask_i.preproc_config=copy.deepcopy(preproc_config)
            mask_i.decompose_config=copy.deepcopy(decompose_config)
            
            mask_i.timestep_i=file_i
            mask_i.read_input_file(fname)

            if (file_i==1):    
                
                self.square_mask= a_math.square_mask(preproc_config.x_1_sq, preproc_config.x_n_sq, preproc_config.y_1_sq, preproc_config.y_n_sq,preproc_config.x_size,preproc_config.y_size)
                
                if (preproc_config.use_fixed_sun_radius=='y'):
                    self.circle_mask= a_math.circle_mask(preproc_config.x_size, preproc_config.y_size, preproc_config.x_size/2, preproc_config.y_size/2, preproc_config.disk_radius)                
                else: 
                    self.circle_mask= a_math.circle_mask(preproc_config.x_size, preproc_config.y_size, mask_i.preproc_config.disk_center_x, mask_i.preproc_config.disk_center_y, mask_i.preproc_config.disk_radius)
                
            mask_i.circle_mask=copy.deepcopy(self.circle_mask)
            mask_i.square_mask=copy.deepcopy(self.square_mask)

            if (file_i==1):
               #mask_i_m1.read_input_file(fname)
               mask_i_m1=copy.deepcopy(mask_i)

            mask_i_proc=self.find_objects_mla_uni(mask_i, mask_i_m1, mask_base) # mask object with structures 
            
            
            use_frame=1
    
            if ( preproc_config.data_lev.find('AIA')>=0):
               if (mask_i.header_orig['AECTYPE']!=0): 
                    use_frame=0

            if (use_frame==1):
            #if (mask_i.header_orig['AECTYPE']==0): 
                
                for struct_i in range (0, mask_i_proc.structs_n): 
                   # write as csv. 

                    line='Structure no:'+str(struct_i+1)+' at '+mask_i_proc.datetime_str+'\n'
                    print(line)
                    results_coord_f.write(line)

                    line='geom_x_cent: '+ str(mask_i_proc.structs_geom_x_cent[struct_i])+'\n'
                    print(line)
                    results_coord_f.write(line)
                
                    line='geom_y_cent: '+ str(mask_i_proc.structs_geom_y_cent[struct_i])+'\n'
                    print(line)
                    results_coord_f.write(line)

                    line='mass_x_cent: '+ str(mask_i_proc.structs_x_cent[struct_i])+'\n'
                    print(line)
                    results_coord_f.write(line)
                
                    line='mass_y_cent: '+ str(mask_i_proc.structs_y_cent[struct_i])+'\n'
                    print(line)
                    results_coord_f.write(line)
                     
                    if ( (decompose_config.search_overlap=='y') and (file_i>1) ): 
                
                        for struct_i in range(0,mask_i_proc.structs_n): 
                            for struct_i_prev in range(0,mask_i_m1.structs_n):
                           
                             
                                do_overlap = mask_i_proc.masks_overlap(mask_i_proc.structs_mask[struct_i],mask_i_m1.structs_mask[struct_i_prev] )                  
                        
                                if (do_overlap==1): 
                                    overlap_count=overlap_count+1

                                    line='Strucutre overlaps with a structure at:'+mask_i_m1.datetime_str+'\n' 
                                     
                                    print(line)
                                    results_overlap_f.write(line) 

                                    line='Structure no:'+str(struct_i_prev)+' at '+mask_i_m1.datetime_str+'\n'
                                    print(line)
                                    results_overlap_f.write(line)

                                    line='geom_x_cent: '+ str(mask_i_m1.structs_geom_x_cent[struct_i_prev])+'\n'
                                    print(line)
                                    results_overlap_f.write(line)
                
                                    line='geom_y_cent: '+ str(mask_i_m1.structs_geom_y_cent[struct_i_prev])+'\n'
                                    print(line)
                                    results_overlap_f.write(line)

                                    line='mass_x_cent: '+ str(mask_i_m1.structs_x_cent[struct_i_prev])+'\n'
                                    print(line)
                                    results_overlap_f.write(line)
                
                                    line='mass_y_cent: '+ str(mask_i_m1.structs_y_cent[struct_i_prev])+'\n'
                                    print(line)
                                    results_overlap_f.write(line)

                                #calc speed
            del mask_i_m1
         
                        
            print('Saving copy of current structures as previos...')            
            mask_i_m1=t_mask.t_mask()
            mask_i_m1=copy.deepcopy(mask_i_proc)
            mask_i_m1.img_init_data=list(mask_i.img_init_data)


            for i in range(0,mask_i_proc.structs_n): 
              
                mask_i_m1.structs_mask[i]=list(mask_i_proc.structs_mask[i]) 
                mask_i_m1.structs_raw[i]=list(mask_i_proc.structs_raw[i])   
                mask_i_m1.structs_x_cent[i]=mask_i_proc.structs_x_cent[i] 
                mask_i_m1.structs_y_cent[i]=mask_i_proc.structs_y_cent[i]
                mask_i_m1.structs_geom_x_cent[i]=mask_i_proc.structs_geom_x_cent[i] 
                mask_i_m1.structs_geom_y_cent[i]=mask_i_proc.structs_geom_y_cent[i]           
 
            del mask_i_proc
            del mask_i    

        results_overlap_f.close()
        results_coord_f.close()



    def __init__(self): 
        
   
        self.decompose_config=t_config.t_decompose_config()
        self.preproc_config = t_config.t_preproc_config()
        

if __name__ == "__main__":
    main()