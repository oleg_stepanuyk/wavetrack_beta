import t_pipeline
import t_config
import t_mask
import os
import sys
import copy
import t_math

def main():
    
    print('Wavetrack Version 1.0 (beta): Console Interface.  A-Trous Wavelet image decomposition and multiscale objects recognition framework. Written by Oleg Stepanyuk. \n')
     
  

    print('Download files for a single event (y/n):')
    response = input()
     
    if response == 'y':
        pipeline = t_pipeline.t_pipeline()
        print('Input begining date-time of the event (YYYY/MM/DD HH:MM:SS)')
        begin_time=input()
        print('Input ending date-time of the event (YYYY/MM/DD HH:MM:SS)')
        end_time=input()
        #add cadency step and wavelength ! 
        pipeline.download_files(begin_time, end_time, 193, 24, '.')
        #bug, saves in python/data folder
        sys.exit()


    decompose_config=t_config.t_decompose_config()
    preproc_config = t_config.t_preproc_config()
    a_math = t_math.t_math()
    

    print('Load custom preprocessing configuration (y/n):')
    response = input()
     
    if response == 'y':
        print('Input filename:')
        fname_preproc=input()
        a_math.set_obj_prop_config_file(preproc_config,fname_preproc)


    print('Load custom decomposition configuration (y/n):')
    response = input()
     
    if response == 'y':
        print('Input filename:')
        fname_decomp=input()
        a_math.set_obj_prop_config_file(decompose_config,fname_decomp)
   
    pipeline = t_pipeline.t_pipeline()

    #pipeline.circle_mask= a_math.circle_mask(preproc_config.x_size, preproc_config.y_size, preproc_config.disk_radius)
    #pipeline.square_mask= a_math.square_mask(preproc_config.x_1_sq, preproc_config.x_n_sq, preproc_config.y_1_sq, preproc_config.y_n_sq,preproc_config.x_size,preproc_config.y_size)

    if (preproc_config.use_base_diff =='y'):
        mask_base_obj=pipeline.base_diff_series( os.getcwd()+preproc_config.base_data_path,preproc_config )
    else: 
        mask_base_obj = t_mask.t_mask()
    
       
    mask_base_obj.preproc_config=copy.deepcopy(preproc_config)
    mask_base_obj.decompose_config=copy.deepcopy(decompose_config)
    
    print('Find objects for a single timestep (y/n):')
    response = input()

    if  response == 'y':
          
        print('Input single timestep filename:')
        fname = input()

        mask_obj = t_mask.t_mask()
        mask_obj.circle_mask=pipeline.circle_mask
        mask_obj.square_mask=pipeline.square_mask
        mask_obj.preproc_config=copy.deepcopy(preproc_config)
        mask_obj.decompose_config=copy.deepcopy(decompose_config)
        mask_obj.read_input_file(fname)
        pipeline.find_objects_mla_uni(mask_obj, mask_base_obj)
        sys.exit()

    
    print('Perform objects recognition for a series of timesteps (y/n):')
    response = input()

    if  response == 'y':
    
        pipeline.track_objects_mla_uni(mask_base_obj, preproc_config, decompose_config)
        sys.exit()     
      



if __name__ == "__main__":
    main()
        
