#!/bin/bash

echo "Wavetrack: Trainset preparation script based oun the Wavetrack output. Oleg Stepanyuk, 2021"

echo "Enter wavetrack output path"
read input_path
echo "Enter first timestep: "
read timestep_num_start
echo "Enter last timestep:"
read timestep_num_end 
echo "Enter timestep interval:"
read timestep_interval
echo "Enter output_path for trainset (randomized):"
read output_path_random
echo "Enter output path for chosen timesteps (filenames remain intact):"
read output_path


# Local paths of wavetrack output and tensorflow trainsets 

vizualize_loc_path="selected_visualize/"
trainset_loc_path="selected_trainset/"
output_loc_path="selected_output/"

tf_raw_input_loc_path="tflow_input_vec_raw/"
tf_diff_input_loc_path="tflow_input_vec_diff/"

tf_raw_output_loc_path="tflow_output_vec_raw/"
tf_diff_output_loc_path="tflow_output_vec_diff/"

tf_raw_input_loc_path_rand="tflow_input_vec_raw_rand/"
tf_diff_input_loc_path_rand="tflow_input_vec_diff_rand/"

tf_raw_output_loc_path_rand="tflow_output_vec_raw_rand/"
tf_diff_output_loc_path_rand="tflow_output_vec_diff_rand/"

# Global paths for selected wavetrack output 
vizualize_glob_path="$output_path$vizualize_loc_path"
trainset_glob_path="$output_path$trainset_loc_path"
output_glob_path="$output_path$output_loc_path"

# Tensorflow input vector global path 

# Tensorflow output vector global paths 
tf_raw_output_glob_path="$output_path$tf_raw_output_loc_path"
tf_diff_output_glob_path="$output_path$tf_diff_output_loc_path"  

# Tensorflow input vector global paths
tf_raw_input_glob_path="$output_path$tf_raw_input_loc_path"
tf_diff_input_glob_path="$output_path$tf_diff_input_loc_path"

# Tensorflow input vector global path with random coefficients 

# Tensorflow output vector global paths (random) 
tf_raw_output_glob_path_random="$output_path_random$tf_raw_output_loc_path"
tf_diff_output_glob_path_random="$output_path_random$tf_diff_output_loc_path"  

# Tensorflow input vector global paths (random)
tf_raw_input_glob_path_random="$output_path_random$tf_raw_input_loc_path"
tf_diff_input_glob_path_random="$output_path_random$tf_diff_input_loc_path"


rm -r $vizualize_glob_path
rm -r $trainset_glob_path
rm -r $output_glob_path

rm -r $tf_raw_input_glob_path
rm -r $tf_diff_input_glob_path
rm -r $tf_raw_output_glob_path
rm -r $tf_diff_output_glob_path

mkdir $vizualize_glob_path
mkdir $trainset_glob_path
mkdir $output_glob_path

mkdir $tf_raw_input_glob_path
mkdir $tf_diff_input_glob_path
mkdir $tf_raw_output_glob_path
mkdir $tf_diff_output_glob_path

mkdir $tf_raw_input_glob_path_random
mkdir $tf_diff_input_glob_path_random
mkdir $tf_raw_output_glob_path_random
mkdir $tf_diff_output_glob_path_random 



timestep_num=$timestep_num_start


while [ $timestep_num -lt $timestep_num_end ] 
do 

    echo "Processing" $timestep_num "timestep"

    random_timestep_num=$RANDOM

    timestep_str_2d=$( printf '%02d' $timestep_num )
    timestep_str_3d=$( printf '%03d' $timestep_num )
    
    random_timestep_str_5d=$( printf '%05d' $random_timestep_num )


    #copying selected wavetrack timesteps from vizualize, trainset and output folders    
    cp ${input_path}visualize/*_t_$timestep_str_3d* $vizualize_glob_path
    cp ${input_path}trainset/*_t_$timestep_str_3d*  $trainset_glob_path
    
    #different Wavetrack versions could have %02d or %03d timestep notation
    cp ${input_path}output_log/out_t_$timestep_str_2d* $output_glob_path >> null
    cp ${input_path}output_log/out_t_$timestep_str_3d* $output_glob_path >> null

    #constructing base-difference based trainset 
    cp ${input_path}trainset/BASE_DIFF_data_t_$timestep_str_3d* $tf_diff_input_glob_path
    cp ${input_path}trainset/masks_applied_BASE_DIFF_data_t_$timestep_str_3d* $tf_diff_output_glob_path
    
    #constructing RAW data based trainset 
    cp ${input_path}trainset/orig_data_t_$timestep_str_3d* $tf_raw_input_glob_path
    cp ${input_path}trainset/masks_applied_orig_data_t_$timestep_str_3d* $tf_raw_input_glob_path

    #random indexes trainsets 
    
    # constructing base-difference based traiset (RANDOM indexes)
    cp ${input_path}trainset/BASE_DIFF_data_t_$timestep_str_3d-?_????-??-?????:??:??.???.fits $tf_diff_input_glob_path_random/BASE_DIFF_data_t_$random_timestep_str_5d.fits
    cp ${input_path}trainset/masks_applied_BASE_DIFF_data_t_$timestep_str_3d-?_????-??-?????:??:??.???.fits $tf_diff_output_glob_path_random/masks_applied_BASE_DIFF_data_t_$random_timestep_str_5d.fits 
    cp ${input_path}trainset/BASE_DIFF_data_t_$timestep_str_3d-?_????-??-?????:??:??.???.png $tf_diff_input_glob_path_random/BASE_DIFF_data_t_$random_timestep_str_5d.png
    cp ${input_path}trainset/masks_applied_BASE_DIFF_data_t_$timestep_str_3d-?_????-??-?????:??:??.???.png $tf_diff_output_glob_path_random/masks_applied_BASE_DIFF_data_t_$random_timestep_str_5d.png 
  

    #constructing RAW data based trainset (RANDOM indexes)
    cp ${input_path}trainset/orig_data_t_$timestep_str_3d_$timestep_str_3d-?_????-??-?????:??:??.???.fits $tf_raw_input_glob_path_random/orig_data_t_$random_timestep_str_5d.fits
    cp ${input_path}trainset/masks_applied_orig_data_t_$timestep_str_3d-?_????-??-?????:??:??.???.fits $tf_raw_input_glob_path_random/masks_applied_orig_data_t_$random_timestep_str_5d.fits
    cp ${input_path}trainset/orig_data_t_$timestep_str_3d_$timestep_str_3d-?_????-??-?????:??:??.???.png $tf_raw_input_glob_path_random/orig_data_t_$random_timestep_str_5d.png
    cp ${input_path}trainset/masks_applied_orig_data_t_$timestep_str_3d-?_????-??-?????:??:??.???.png $tf_raw_input_glob_path_random/masks_applied_orig_data_t_$random_timestep_str_5d.png

    declare -i timestep_num=$timestep_num+$timestep_interval

done



