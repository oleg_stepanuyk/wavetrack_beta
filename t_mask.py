import t_atrous_wavelet 
import t_atrous_postprocess
import t_math
import numpy as np
import matplotlib.pyplot as plt
from scipy import ndimage
from scipy.stats import norm
import random
import os
import gc



class t_mask(t_atrous_postprocess.t_atrous_postprocess):
#Clustering and multi-level objects binding partially ported from iAthena7 GeoWizard


    def convolve_axes_kernels(self,data,g_x_kernel,g_y_kernel):
    # A function to convolve the list with two kernels 
     
        a_math = t_math.t_math()
        data_float=a_math.bring_to_one_float64(data)
        g_x=ndimage.convolve(np.array(data_float),np.array(g_x_kernel), mode='reflect')   
        g_y=ndimage.convolve(np.array(data_float),np.array(g_y_kernel), mode='reflect')
        g_x_sq=np.multiply(g_x,g_x)
        g_y_sq=np.multiply(g_y,g_y)
        sum_sq=np.add(g_x_sq,g_y_sq)
        g = np.sqrt(sum_sq)
        g_int=a_math.bring_to_int32(g)
        return g_int,np.arctan(np.divide(g_y,g_x))


    def truncate_sigmas(self,data,thrhold):
      
        data_arr=np.array(data)
        fitted_mean,fitted_std=norm.fit(data_arr) 
        arr_trunc = (data_arr>fitted_std)*data_arr
        return arr_trunc
             

    def decompositions_sobel_feldman(self):
        #calculating gradients with Sobel-Feldman operator for all wavelet coefficients/convolions and cutting off everything below
        #grad_sigma_thrhold

        print('Calculating gradients with Sobel-Feldman operator...')
      
        for scale_i in range(0,self.decompose_config.scales_n_max):       
    
            print('Scale:'+str(scale_i))
    
            sobel_j_arr,angle  = self.convolve_axes_kernels(self.decompositions[scale_i],self.kernel_sobel_gx, self.kernel_sobel_gy)
            sobel_j_arr_trunc = self.truncate_sigmas(sobel_j_arr,self.decompose_config.grad_thrhold_sigma)
            self.decompositions_grads.append(sobel_j_arr_trunc)
             

    def decompositions_simple_grad(self):
        print('Calculating gradients with [-1, 0, 1] x [-1, 0, 1] kernel...')
 
        for scale_i in range(0,self.decompose_config.scales_n_max):       
            print('Scale:'+str(scale_i))
            grad_j_arr_=ndimage.convolve(np.array(self.decompositions[scale_i]),np.array(self.kernel_grad), mode='reflect')
            self.decompositions_grads.append(grad_j_arr)
           
                      
    def calc_curv_xi(self,data,i,j,h_1,h_2):
    # Curvature "xi" as described by Hagenaer et. al. 1999 Astr.J. 511:932-944,1999         
        return (2*data[i][j]-data[i+h_1][j+h_2]-data[i-h_1][j-h_2])


    def calc_contours_hagenaer(self,data,bold): 
         # As described by Hagenaer et. al. 1999 Astr.J. 511:932-944,1999       
         #(h_1 , h_2)  (1, 0), (1, 1), (0, 1), (-1, 1)  
        i_n = len(data[0])
        j_n = len(data)
        
        contours = [[0 for i in range(i_n)] for j in range(j_n)]
    
        for i in range(1,i_n-1):
             for j in range(1,j_n-1):
               
                if (1):
                     
                    counter=0
                    counter_xi=0
                     
                    for h_1 in range(0,2):
                        for h_2 in range(0,2):
                            counter=counter+1
                             
                            if ( (self.calc_curv_xi(data,i,j,h_1,h_2)>0) and ( (h_1+h_2)>0)): 
                                counter_xi=counter_xi+1

                    counter=counter+1 
                    if (self.calc_curv_xi(data,i,j,-1,1))>0: 
                        counter_xi=counter_xi+1


                    if (counter_xi>3):
                        contours[i][j]=1 
                        
                        if (bold):
                           #According to the same criteria making borders less thin    
                           for h_1 in range(0,2):
                               for h_2 in range(0,2):
                                   contours[i+h_1][j+h_2] = 1
                                   contours[i-h_1][j-h_2] = 1
        return contours                                   
                      


    def decompositions_hagenaer(self):
     
        print('Calculating gradients and contours with curvature criteria...')
        for scale_i in range(0,self.decompose_config.scales_n_max): 
            self.decompositions_grads.append(self.calc_contours_hagenaer(self.decompositions[scale_i],0))
 
    
    def estimate_saddle_count_stochastic(self,data,data_grad,structure_size,thrhold_sigmas):
        # Monte-Carlo algorithm

        i_size=len(data[0]) #columns
        j_size=len(data) #rows 
     
        i_structure_size=round(np.sqrt(structure_size)) 
        j_structure_size=round(np.sqrt(structure_size))
        
        mc_step_i_n = round((i_size/i_structure_size))*8
        mc_step_j_n = round((j_size/j_structure_size))*8  
        intensities=[]
        intensities_sort = []
               
        fitted_mean,fitted_std=norm.fit(np.array(data)) 

        random.seed(a=None, version=2)

        for mc_step_i_m in range(0,int(mc_step_i_n*mc_step_j_n)):

                i= random.randint(0,(i_size-1))
                j= random.randint(0,(j_size-1))
                
                if ( (data_grad[i][j]==0)) and (data[i][j]>fitted_std):
                    
                    try:
                           
                            this_intensity_i=intensities.index(data[i][j])  
                        
                    except ValueError:

                            intensities.append((data[i][j]))

        intensities_sort=intensities.sort()
        intensities_sort_n = len(intensities)
        print('Estimated different intensity fields count:'+str(intensities_sort_n))
 
        return intensities_sort, intensities_sort_n
    
       
    def calc_optimal_w_coeff(self,structure_size,thrhold_sigmas): 
        
        print('Trying to determine optimal scale based on the structure minimum size...')
      
        for scale_i in range(0,self.decompose_config.scales_n_max):              
            print('Trying scale:'+str(scale_i)) 
            print(scale_i)
            intensities_sort,intensities_sort_n=self.estimate_saddle_count_stochastic(self.decompositions[scale_i],self.decompositions_grads[scale_i],structure_size,thrhold_sigmas*self.decompositions_std[scale_i])          
    
    
    def boolenize_data_unambiguos(self, data):

        result = [[0 for col_i in range(len(data))] for row_i in range(len(data[0]))];      
        result = np.logical_not(data == 0)

     
    def binarize_data(self, data, thrhold): 

        data_arr=np.array(data) 
        #result= (data_arr>thrhold)
        result= (data_arr!=0)
        return result

  
    def cluster_mask_rows_pass(self,data):
          
       #works only with binary data !
   
       i_n = len(data) # number of rows 
        
       struct_inc=0 
      
       max_struct_num=0 
       result=data
       i_arr_max_prev=10000 
       data_bord=np.array(data)
       data_bord[0]=np.zeros(i_n)
       data_bord[1]=np.zeros(i_n)
       data_bord[2]=np.zeros(i_n)
       data_bord[i_n-1]=np.zeros(i_n)
       data_bord[i_n-2]=np.zeros(i_n)
       data_bord[i_n-3]=np.zeros(i_n)

       for i in range(1,i_n):
           
           i_arr=np.array(data_bord[i])
           i_arr_max=np.amax(i_arr)
           
           if ((((i_arr_max==0) and (i_arr_max_prev>0)))): #or (i>=1)) :
              struct_inc = struct_inc+1
               
           i_arr_p = np.add(i_arr,struct_inc)
           i_arr= (i_arr>0)*(i_arr_p)
            
           result[i]=list(i_arr)
         

           if (max_struct_num<np.amax(i_arr)):
              max_struct_num=np.amax(i_arr)
            
           i_arr_max_prev=i_arr_max

       return result, struct_inc #max_struct_num            
      
    
    def get_AND_super_pixel(self, data, x, y, thikness):
        
        x_n = len(data[0])
        y_n = len(data)  

        for x_inc in range( (0-thikness), thikness+1): 
           for y_inc in range ( (0-thikness), thikness+1):

	           x_1=x+x_inc 
	           y_1=y+y_inc 

	           if ( (x_1!=x) and (y_1!=0) and (x_1>0) and (x_1<x_n) and (y_1>0) and (y_1<y_n)): 
	              if (data[x_1][y_1]!=0):
	                 return data[x_1][y_1]

        return 0 


    def get_OR_super_pixel(self, data, x, y, thikness):  
        
        x_n = len(data[0])
        y_n = len(data)  

        for x_inc in range( (0-thikness), thikness+1): 
            for y_inc in range ( (0-thikness), thikness+1):

	            x_1=x+x_inc 
	            y_1=y+y_inc 

	            if ( (x_1>0) and (x_1<x_n) and (y_1>0) and (y_1<y_n)): 
	                if (data[x_1][y_1]!=0):
	                    return data[x_1][y_1]  # add function with averages 
                      
 
        return 0 
 


    def get_avg_super_pixel(self,data,x,y,thikness):
        
        x_n = len(data[0])
        y_n = len(data)
        sum_pix = 0 
        sum_pix_n=0

        for x_inc in range( (0-thikness), thikness+1): 
            for y_inc in range ( (0-thikness), thikness+1):
               
                x_1=x+x_inc 
                y_1=y+y_inc 

                if ( (x_1>0) and (x_1<x_n) and (y_1>0) and (y_1<y_n)): 
                    sum_pix = sum_pix + data[x_1][y_1]  # add function with averages 
                    sum_pix_n = sum_pix_n+1       
	    
        sum_pix=sum_pix/sum_pix_n 
        return sum_pix 



    def calc_superpix_data(self, data, thikness,superpixel_mode):  

        x_n = len(data[0])
        y_n = len(data)
       
        x_s_n = round(len(data[0])/(thikness*2))
        y_s_n = round(len(data)/(thinkess*2))  

        superpix_small_data = [[0 for col_i in range(x_s_n)] for row_i in range(y_s_n)]
        superpix_orig_data =  [[0 for col_i in range(x_n)] for row_i in range(y_n)]                                     

        for x_i in range(0,x_n, thikness*2):         
 
            for y_i in range(0,y_n, thikness*2):

                x_i_cent = x_i-thikness 
                y_i_cent = y_i-thikness

                if (superpixel_mode=='OR'):
                    pix_value = get_OR_super_pixel(data, x_i_cent, y_i_cent, thikness)   
                elif (superpiel_mode=='AVG'):
                     pix_value = get_OR_super_pixel(data, x_i_cent, y_i_cent, thikness)  

                superpix_small_data[x_s_i][y_s_i]=pix_value
                y_s_i=y_s_i+1 


            x_s_i=x_s_i+1 


        for x_s_i in range(1, x_s_n):
            for y_s_i in range(1, y_s_n):
                
                x_i_cent=x_s_i*thikness*2-thikness
                y_i_cent=y_s_i*tinkness*2-thikness 

                for x_inc in range( (0-thikness), thikness+1): 
	                for y_inc in range ( (0-thikness), thikness+1):

	                    x_i=x_i_cent+x_inc 
	                    y_i=y_i_cent+y_inc
	                    superpix_orig_data[x_i][y_i]=superpix_small_data[x_s_i][y_s_i] 

             

        return superpix_orig_data, superpix_small_data    
  


    def if_belong_same_struct(self, data,x_1,y_1,x_2,y_2,check_points,check_value):
         
        t=1/check_points  
        done_checks=0

        while (done_checks<check_points): 
        
            x=x_1+round((x_2-x_1)*(t*done_checks))
            y=y_1+round((y_2-y_1)*(t*done_checks)) 
            done_checks=done_checks+1             

            if (data[x][y]==check_value): 
            	return 1


        return 0


  
    def calc_mass_center(self, data, inc): 
        # as the case is discrete, the indegral coverges to the sum 
        x_cent = 0 
        y_cent = 0 
        x_n = len(data[0])
        y_n = len(data)
        m_s = 0
        
        for x in range(1,x_n,inc):
            for y in range(1,y_n,inc):
              
               
                data_xy=data[x][y]
                if (data_xy!=data_xy):
                    print('Warning, mass center calculation is inconsistent: data[x][y]==NaN')
                    data_xy=0 

                x_cent=x_cent+x*data_xy
                y_cent=y_cent+y*data_xy
                m_s=m_s+data_xy
              
      
        if ((m_s==0)):
            print('Warning, mass center calculation is inconsistent: m_s=m_s+data[x][y] == 0 -> all data elements are zeros ! ')
            x_cent=0
            y_cent=0

        else:
            x_cent=int(round(x_cent/m_s))
            y_cent=int(round(y_cent/m_s))
        
        return x_cent,y_cent

   

    def structures_masks(self,path,data_raw,data,param_1,sigma_thrhold,min_struct_size):
         
        # rot90(m, k=1, axes=(1,0)) is the reverse of rot90(m, k=1, axes=(0,1))
        a_math=t_math.t_math()

        print('Trying to obtain structures masks:')
    
        fitted_mean,fitted_std=norm.fit(np.array(data)) 
        data_raw_segment=np.multiply(data_raw,self.square_mask)
        binary_data=self.binarize_data(data_raw_segment, fitted_std*sigma_thrhold) 
        #here here add small features remover with window.
        
        #a_math.save_data_pic(binary_data,path+'/binary_data_test.png',self.preproc_config.x_size,self.preproc_config.y_size)
        #a_math.save_data_pic(data_raw,path+'/raw_data_test.png',self.preproc_config.x_size,self.preproc_config.y_size)
           
        structs_combined_mask_arr = np.zeros((self.preproc_config.x_size, self.preproc_config.y_size)) 
        structs_combined_raw_arr = np.zeros(( self.preproc_config.x_size, self.preproc_config.y_size)) 

        if (self.decompose_config.use_abc_mask == 'y'):
            cluster_mask,max_struct_num_2 = self.abc_mask_2(binary_data,sigma_thrhold*fitted_std)  
            print('ABC mask, structures found:', max_struct_num_2) 

        else:

            print('Clustering approach (works for low noize images with less details...:')

            mask_rows,max_struct_num = self.cluster_mask_rows_pass(binary_data) 
            print('Row pass, structures found:', max_struct_num) 
            a_math.save_data_pic(binary_data,path+'/mask_rows_test.png',self.preproc_config.x_size,self.preproc_config.y_size)
       
          
            mask_rows_arr=np.array(mask_rows)
            mask_rows_arr_90 = np.rot90(mask_rows_arr,k=1,axes=(0,1))
            mask_rows_arr_90_2,max_struct_num_2 = self.cluster_mask_rows_pass(list(mask_rows_arr_90))
            cluster_mask = np.rot90(mask_rows_arr_90_2, k=1, axes=(1,0))
            print('Column pass, structures found:', max_struct_num_2) 
            a_math.save_data_pic(binary_data,path+'/mask_columns_test.png',self.preproc_config.x_size,self.preproc_config.y_size)
            
        
            #cluster_mask=a_math.window_filt(cluster_mask,20, 2, 0, 1000, 1000)
        
        struct_i_mask = np.multiply(np.array(data),0)
        struct_raw_i_mask = np.multiply(np.array(data_raw),0)
        structs_n=0
        
        for struct_i in range(1,max_struct_num_2+1):
            
            struct_i_mask = (cluster_mask==struct_i) 
            struct_raw_i_mask = (cluster_mask==struct_i)*data_raw
            struct_size =np.count_nonzero(struct_i_mask) 

            if (struct_size>min_struct_size):  
                
                self.save_separate_structure(path,struct_raw_i_mask,struct_i_mask,  param_1, struct_i+1)     
                self.structs_n=self.structs_n+1
                self.structs_raw.append(list(struct_raw_i_mask))
                self.structs_mask.append(list(struct_i_mask))
                self.structs_intensity_i.append(param_1)
                #self.structs_scale_i.append(scale_i) 
                print('Determining structures center of the mass...:')
                x_cent,y_cent=self.calc_mass_center(list(struct_raw_i_mask),4)
                x_geom_cent,y_geom_cent=self.calc_mass_center(list(struct_i_mask),4)
                self.structs_geom_x_cent.append(x_geom_cent)
                self.structs_geom_y_cent.append(y_geom_cent)
                self.structs_x_cent.append(x_cent)
                self.structs_y_cent.append(y_cent)
                print('x_cent, y_cent: ' + str(x_cent)+','+str(y_cent))
                structs_combined_mask_arr = np.add(structs_combined_mask_arr, struct_i_mask )   
                structs_combined_raw_arr =  np.add(structs_combined_raw_arr, struct_raw_i_mask) 
                

            elif (struct_size>0):
                print('Omitting structure, as it consists of '+str(struct_size)+' pixels, which is below '+str(min_struct_size)+' pixels')

            
        self.structs_combined_mask = list(structs_combined_mask_arr)   
        self.structs_combined_raw =  list(structs_combined_raw_arr)

        print('Saving combined mask build from structures above chosen pixel size')
        self.save_separate_structure(path, structs_combined_raw_arr, structs_combined_mask_arr, param_1, 0)
        #self.save_data(path,'structs_raw', structs_combined_raw_arr, param_1,0,'y')
        #self.save_data(path,'structs_masks', structs_combined_mask_arr , param_1,0,'y')
             
        print('Applying combined mask to the original data for the AI training set... ') 
        img_init_data_orig_arr= np.array(self.img_init_data_orig)
        structs_combined_orig_data_arr = np.multiply( img_init_data_orig_arr, structs_combined_mask_arr  )
        self.structs_combined_orig_data = list(structs_combined_orig_data_arr)
        self.save_data(path,'obj_area_orig', structs_combined_orig_data_arr, param_1,0,'n')



    def apply_combined_structs_mask(self): 

        print('Applying chosen structures combined mask to the data...')
        for j in range(0,self.decompose_config.scales_n_max):            
         
            self.decompositions[j] = list (np.multiply(np.array(self.decompositions[j]), np.array(self.structs_combined_mask)))
            self.decompositions_raw[j] = list (np.multiply(np.array(self.decompositions_raw[j]), np.array(self.structs_combined_mask)))
            self.decompositions_grads[j] = list (np.multiply(np.array(self.decompositions_grads[j]), np.array(self.structs_combined_mask)))
            self.decompositions_grads_ref[j] = list(np.multiply(  np.array(self.decompositions_grads_ref[j]), np.array(self.structs_combined_mask)))
          
              
        
    def struct_i_2_object_mla(self,path,struct_i): # Extracting objects from structures according to Multilevel Data representation Approach
                                                   # depending on the maximum value of the wavelet coefficient on j+1 and j-1 wavelet 
                                                   # decomposition levels (See SEXTractor)
        
          struct_i_x_cent=self.structs_x_cent[struct_i]
          struct_i_y_cent=self.structs_y_cent[struct_i]
          struct_i_scale_i  = self.structs_scale_i[struct_i]
          struct_i_raw_arr = np.array(self.structs_raw[struct_i]) 
          struct_i_size =np.count_nonzero(struct_i_raw_arr) 
          struct_i_max = np.amax(struct_i_raw_arr)

          #by default assume that structure represents the object on it's decomposition level           
          print('Analyzing structure no:'+ str(struct_i))
          prev_lev_less_magn = 1 
          next_lev_less_magn = 1  

          # tryin to figure out if otherwise, ...:

          for struct_k in range(0,self.structs_n):

              struct_k_scale_i = self.structs_scale_i[struct_k]
          
              #  Warning : check if max wavelet coefficient belongs to the structure on a level !
              if ((self.structs_intensity_i[struct_k]==self.structs_intensity_i[struct_i])): 

                 struct_k_raw_arr = np.array(self.structs_raw[struct_k]) 
                 struct_k_size =np.count_nonzero(struct_k_raw_arr) #ACHTUNG ADDITIONALLY CHECK through comparing centers dist and sqrt of size 
                 struct_k_max = np.amax(struct_k_raw_arr)
                 scale_diff=struct_k_scale_i-struct_i_scale_i
                 
                 if ((self.structs_mask[struct_k][struct_i_x_cent][struct_i_y_cent]!=0) and (struct_k_max>struct_i_max) and (scale_diff==1) and
                 (struct_k_max>struct_i_max) and (scale_diff==1)):
                     #next decomposition level
                     print('Omitting, next (j+1) decomposition level has larger value of the maximum wavelet coefficient')
                     next_lev_less_magn=0
                 
                 if ((self.structs_mask[struct_k][struct_i_x_cent][struct_i_y_cent]!=0) and (struct_k_max>struct_i_max) and (scale_diff==(-1)) and
                 (struct_k_max>struct_i_max) and (scale_diff==1)):
                     #next decomposition level
                     print('Omitting, next (j-1) decomposition level has larger value of the maximum wavelet coefficient')
                     prev_lev_less_magn=0

          
          if ((prev_lev_less_magn==1) and (next_lev_less_magn==1)): 
          
              print('Structure fulfills the criteria....') 
              
              skip_object = 0 

              for object_l in range(0,self.objects_n):
                   
                  if ((self.objects_raw[object_l][struct_i_x_cent][struct_i_y_cent])!=0) and (self.objects_scale_i[object_l]>self.structs_scale_i[struct_i] ):
                      print('Omitting: object exists on a more detailed decomposition level...')
                      skip_object=1 


              if (skip_object==0):
                  self.objects_n=self.objects_n+1                 
                  self.objects_raw.append(self.structs_raw[struct_i])
                  self.objects_mask.append(self.structs_mask[struct_i])
                  self.objects_scale_i.append(self.structs_scale_i[struct_i])
                  self.objects_intensity_i.append(self.structs_intensity_i[struct_i])
                  self.objects_x_cent.append(self.structs_x_cent[struct_i])
                  self.objects_y_cent.append(self.structs_y_cent[struct_i])
                  self.objects_geom_x_cent.append(self.structs_geom_x_cent[struct_i])
                  self.objects_geom_y_cent.append(self.structs_geom_y_cent[struct_i])
                  self.objects_ids.append(self.objects_n)
                  print('Adding object:'+str(self.objects_n+1))
                  self.save_separate_object(path,self.objects_raw[self.objects_n-1],self.objects_mask[self.objects_n-1],self.objects_intensity_i[self.objects_n-1],self.objects_n-1)


    def structs_2_objects_mla(self,path):
        print('Browsing through all structures to find objects:')
        print('self.structs_n:')
        print(self.structs_n)
        for struct_i in range(0,self.structs_n):
            self.struct_i_2_object(path,struct_i)

    
    def masks_overlap(self,data_1,data_2): 
        
        data_sum_arr= np.add(np.array(data_1),np.array(data_2))

        data_sum_arr_max=np.amax(data_sum_arr)
        data_1_max=np.amax(np.array(data_1))
        data_2_max=np.amax(np.array(data_2))

        if ((data_sum_arr_max>data_1_max) and (data_sum_arr_max>data_2_max)): 
           return 1 

        else: 
           return 0 



    def abc_mask_2(self, data,low_threshold): 
          
        x_n = len(data[0])
        y_n = len(data)
        struct_i = 0 
        result_masks=(data>low_threshold) 


        for x_i in range(2,x_n):
           for y_i in range(2,y_n):
              
              pix_a = result_masks[x_i][y_i]
              pix_b = result_masks[x_i-1][y_i]
              pix_c = result_masks[x_i][y_i-1]
              
              if ( (pix_a==0) and (pix_b==0) and (pix_c==0)):
              	  #do nothing
                  nul=0 

              elif ( (pix_b==0) and (pix_c==0) and (pix_a!=0)): 
                    struct_i=struct_i+1
                    result_masks[x_i][y_i]=struct_i 
              elif ( (pix_b!=0) and (pix_c==0) and (pix_a==0)):  
                    result_masks[x_i][y_i]=pix_b
              elif ( (pix_b==0) and (pix_c!=0) and (pix_a==0)):
                    result_masks[x_i][y_i]=pix_c
              elif ( (pix_b!=0) and (pix_c!=0) and (pix_a!=0)): 
                    result_masks[x_i][y_i]=pix_b
              	      
              	
        return result_masks, struct_i 

    
  
    def abc_mask(self,path,data_raw,data,intensity_i):
        
        i_n = len(data[0])
        j_n = len(data)
        # loop through all pixels 
      
        result = data
        struct_n = 0
        # achtung - experiment with result and data, 
        A=0
        B=0
        C=0
        fitted_mean,fitted_std=norm.fit(np.array(data)) 

        for i in range(1,i_n):
           for j in range(1,j_n):
             
               kn = j - 1
    
               if (kn <= 0):
               
                  kn = 1
                  B = 0
                
               else: 
               
                  B = result[i][kn] # case 3
         
               km = i - 1
    
               if (km <= 0):

                  km = 1
                  C = 0
               
               else:
               
                  C = result[km][j] # case 3
    
               A = result[i][j] # case 3
               
               #if A == 0 then // do nothing 
    
               if ((B < fitted_std ) and (C <fitted_std)):  # New object 
                   struct_n = struct_n + 1
                   result[i][j] = struct_n
        
               elif ( ( B!=0 ) and (C == 0) ):
         
                   result[i][j] = B
              
               elif ( (B == 0) and  (C != 0) ): 
                   result[i][j] = C
        
               elif ( (B != 0) and (C != 0) ):       
        
                  if (B==C):
                     result[i][j] = B
                  
                  #else:
                    # result[i][j] = B
                     #Image(Image == C) = B;
        
        
   
        for struct_i in range(0,struct_n):
            
            struct_i_data = (result==struct_i)*result
            struct_raw_i_data = (result==struct_i)*data_raw
            self.save_separate_structure(path,struct_raw_i_data,struct_i_data, intensity_i, struct_i)

            
    '''


Alternatively for non-intensity depending labels numbering, port recursive function from GeoWizard:

void Labeling(BIT* img[], int* labels[])
{
 int L = 1;
 for(int y = 0; y < H; ++y)
  for(int x = 0; x < W; ++x)
   Fill(img, labels, x, y, L++);
}
void Fill(BIT* img[], int* labels[], int x, int y, int L)
{
  if( (labels[x][y] == 0) & (img[x][y] == 1) )
  {
    labels[x][y] = L;
    if( x > 0 )
        Fill(img, labels, x – 1, y, L);
    if( x < W — 1 )
        Fill(img, labels, x + 1, y, L);
    if( y > 0 )
        Fill(img, labels, x, y — 1, L);
    if( y < H — 1 )
        Fill(img, labels, x, y + 1, L);
  }
}
   '''    


    def extract_intensities_masks(self,path,data_raw,data,data_grad,scale_i,sigma_thrhold,grad_sigma_thrhold,min_struct_size): 
    #split image into different intensity levels/masks

        i_n = len(data[0])
        j_n = len(data)
        intensity_values_detected=[]
        intensity_n = 0       

        fitted_mean,fitted_std=norm.fit(np.array(data)) 
        fitted_mean_grad,fitted_std_grad=norm.fit(np.array(data_grad)) 

        for i in range(1,i_n-1):
             for j in range(1,j_n-1):
               
                if ( (data[i][j]>fitted_std*sigma_thrhold) and (data_grad[i][j]<=fitted_std_grad*grad_sigma_thrhold) )  :
                #if ( (data[i][j]>fitted_std) and (data_grad[i][j]==0)):
                                              
                        try:
                            
                            found_intensity_n=intensity_values_detected.index(data[i][j])  
                        
                        except ValueError:
                        #not found 
              
                            intensity_values_detected.append(data[i][j])
                            intensity_n=intensity_n+1
                            print('Found another intensity field:'+str(intensity_n))
                            print('Value:'+str(data[i][j]))
                          
                            

        for intensity_i in range (0, intensity_n):
            
            print('Extracting intensity field mask no:'+str(intensity_i))           
            intensity_i_mask = (data==intensity_values_detected[intensity_i])*data
            intensity_i_raw_mask = (data==intensity_values_detected[intensity_i])*data_raw
            self.save_intensities_mask(path,intensity_i_raw_mask,intensity_i_mask,intensity_i)               
            self.structures_masks(path,intensity_i_raw_mask,intensity_i_mask,intensity_i,scale_i,0,min_struct_size)                    



    def apply_reference_mask_grads(self):
        
        reference_mask_arr = np.array(self.reference_mask)
        for j in range(0,self.decompose_config.scales_n_max):            
            decompositions_grads_arr=np.array(self.decompositions_grads[j])
            decompositions_grads_ref_arr = np.multiply(decompositions_grads_arr,reference_mask_arr)
            self.decompositions_grads_ref.append(list(decompositions_grads_ref_arr))
    

    def bind_2_layers_XOR(self, data_init, data_XOR): 


        data_init_arr = np.array(data_init)
        data_XOR_arr = np.array(data_XOR)
                
        data_init_region_arr = data_init_arr * (data_XOR_arr>0)    
        data_init_hole_arr = np.subtract(data_init_arr, data_init_region_arr)
   
        result = np.add(data_init_hole_arr,data_XOR_arr)
        return result


    def bind_2_layers_replace(self, data_init, data_XOR): 


        data_init_arr = np.array(data_init)
        data_XOR_arr = np.array(data_XOR)
                
        data_init_region_arr = data_init_arr * (data_XOR_arr>0)    
        data_init_hole_arr = np.subtract(data_init_arr, data_init_region_arr)
   
        result = np.add(data_init_hole_arr,data_XOR_arr)
        return result
        


    def bind_2_layers_region(self, data_init, data_XOR): 

        data_init_arr = np.array(data_init)
        data_XOR_arr = np.array(data_XOR)
        data_init_region_arr = data_init_arr * (data_XOR_arr>0)    
        return data_init_region_arr
        

    def save_decomposition_grads(self,path):
        
        a_math=t_math.t_math()

        for j in range(0,self.decompose_config.scales_n_max):
           
            print('Saving gradient field:'+str(j))              
            
            a_math.save_data_pic(self.decompositions_grads[j][:],path+'/grad_'+'t_'+str(self.timestep_i)+'_'+self.datetime_str+'_wlgth_'+str(self.wavelength)+'_scale_'+str(j)+'.png',self.preproc_config.x_size,self.preproc_config.y_size)
            grad_j_norm=a_math.bring_to_int32(self.decompositions_grads[j])
            a_math.save_data_pic(grad_j_norm,path+'/grad_norm'+'t_'+str(self.timestep_i)+'_'+self.datetime_str+'_wlgth_'+str(self.wavelength)+'_scale_'+str(j)+'.png',self.preproc_config.x_size,self.preproc_config.y_size)
            plt.imsave(arr = self.decompositions_grads_ref[j][:], fname=path+'/grad_ref'+'t_'+str(self.timestep_i)+'_'+self.datetime_str+'_wlgth_'+str(self.wavelength)+'_scale_'+str(j)+'.png')    
            

    def save_intensities_mask(self,path,data_raw,data,intensity_i):
      
        print('Saving intensity:'+str(intensity_i))
        plt.imsave(arr = data_raw, fname=path+'/intensity_raw_'+'t_'+str(self.timestep_i)+'_'+str(intensity_i)+self.datetime_str+'.png')
        plt.imsave(arr = data, fname=path+'/intensity_'+'t_'+str(self.timestep_i)+'_'+str(intensity_i)+self.datetime_str+'.png')
      

    def save_separate_structure(self,path,data_raw,data, param_1, param_2):
        #save in binary database as well ... 
        print('Saving separate structure:'+str(param_1))  
        plt.imsave(arr = data, fname=path+'/mask_structure_'+'t_'+str(param_1)+'-'+str(param_2)+'_'+self.datetime_str+'.png')
        plt.imsave(arr = data_raw, fname=path+'/structure_raw_'+'t_'+str(param_1)+'-'+str(param_2)+'_'+self.datetime_str+'.png')
         

    def save_separate_object(self,path,data_raw,data, intensity_i, object_i):         
        print('Saving separate object:'+str(object_i))
        plt.imsave(arr = data, fname=path+'/mask_object_'+'t_'+str(self.timestep_i)+'_'+str(self.objects_ids[object_i])+'-'+str(intensity_i)+'_'+self.datetime_str+'.png')
        plt.imsave(arr = data_raw, fname=path+'/mask_object_raw_'+'t_'+str(self.timestep_i)+'_'+str(self.objects_ids[object_i])+'-'+str(intensity_i)+'_'+self.datetime_str+'.png')
    
    
    def calc_avg_grad(self): 
        
        print('Trying to determine optimal scale based on the structure minimum size...')    
        i_n = len(self.decomposition_grads[0])
        j_n = len(self.decomposition_grads)
        restored_img_arr = np.zeros((i_n, j_n))
        cumul_weights = 0

        for j in range(0,self.decompose_config.scales_n_max):
              
            grads_j_arr=np.array(self.decomposition_grads[j],dtype='int32')
            grads_j_weights_arr=np.multiply(grads_j_arr, self.decompose_config.scales_weights[j])
            cumul_weights = cumul_weights + self.decompose_config.scales_weights[j]
            avg_grad_arr=np.add(avg_grad_arr,grads_j_weights_arr)

        avg_grad_arr = np.divide(avg_grad_arr, cumul_weights )

        self.avg_grad=list(avg_grad_arr)
        self.decompositions_grads.append(self.avg_grad)



    def __init__(self): 

       t_atrous_postprocess.t_atrous_postprocess.__init__(self)
       self.quantize_rate = 10 # depricated 
       self.laptop_mode = 0 # memory saving mode 


       self.kernel_gauss_5 = [[1 / 256, 4  / 256,  6 / 256,  4 / 256, 1 / 256],
                             [4 / 256, 16 / 256, 24 / 256, 16 / 256, 4 / 256],
                             [6 / 256, 24 / 256, 36 / 256, 24 / 256, 6 / 256],
                             [4 / 256, 16 / 256, 24 / 256, 16 / 256, 4 / 256],
                             [1 / 256, 4  / 256,  6 / 256,  4 / 256, 1 / 256]]

    
       self.kernel_gauss_3 = [[1/16,1/8,1/16],
                             [1/8,1/4,1/8],
                             [1/16,1/8,1/16]]   


       self.kernel_box = [[1 / 9, 1 / 9, 1 / 9],
                         [1 / 9, 1 / 9, 1 / 9],
                         [1 / 9, 1 / 9, 1 / 9]]
    
       self.kernel_contour_hard = [[-1,-1,-1],
                                  [-1,8,-1],
                                  [-1,-1,-1]]

       self.kernel_grad_1d = [-1, 0, 1]

       # Sobel-Feldman operator for contours as defined in: 
       # Sobel I., Feldman G. «A 3x3 Isotropic Gradient Operator for Image Processing», 1968.
       # Duda R., Hart P. Pattern Classification and Scene Analysis. — John Wiley and Sons, 1973

       self.kernel_sobel_gx= [[1,0,-1],
                             [2,0,-2],
                             [1,0,-1]]

       self.kernel_sobel_gy= [[1,2,1],
                             [0,0,0],
                             [-1,-2,-1]]
   
       self.kernel_grad = [[1,0,-1],
                          [0,0,0],
                          [-1,0,1]]
       
       self.decompositions_grads = []
       self.decompositions_grads_ref = []

       self.avg_grad = []

       self.intensities = []
    
       self.structs_raw = []
       self.structs_mask = []
       self.structs_scale_i = []
       self.structs_intensity_i = []
   
       self.structs_x_cent = []
       self.structs_y_cent = []
       self.structs_geom_x_cent = []
       self.structs_geom_y_cent = []
       self.structs_n = 0
       
       self.objects_raw = []
       self.objects_mask = []
       self.objects_scale_i = []
       self.objects_intensity_i = []
   
       self.objects_x_cent = []
       self.objects_y_cent = []
       self.objects_geom_x_cent = []
       self.objects_geom_y_cent = []
       self.objects_ids = []

       self.objects_n = 0
    
    
        
