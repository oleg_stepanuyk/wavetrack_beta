import numpy as np
import numpy
from scipy import ndimage
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import numpy as np
import io
from PIL import Image, ImageOps
import sunpy.map


class t_math(object):
#various substitute math and statistical routines   

    
    # get maximum value for 2D list / table
    def get_max(self,my_list):
        
        m = None
        for item in my_list:
            
            if isinstance(item, list):
                item = get_max(item)
            
            if not m or m < item:
                m = item
            
        return m 


    # Gaussian 
    def gauss(self,x, *p):
        A, mu, sigma = p
        return A*numpy.exp(-(x-mu)**2/(2.*sigma**2))
        # Define some test data which is close to Gaussian
    

    
    def gauss_fit(self,data):
    # Gaussian fit
  
        hist, bin_edges = numpy.histogram(data, density=True)
        bin_centres = (bin_edges[:-1] + bin_edges[1:])/2

        # p0 is the initial guess for the fitting coefficients (A, mu and sigma above)
        p0 = [1., 0., 1.]

        coeff, var_matrix = curve_fit(self.gauss, bin_centres, hist, p0=p0)
        # Get the fitted curve
        hist_fit = self.gauss(bin_centres, *coeff)

        # Finally, lets get the fitting parameters, i.e. the mean and standard deviation:
        #fitted_mean=coeff[1] # Fitted mean value
        #fitted_std=coeff[2] # fitted std. deviation 
  
        fitted_mean,fitted_std=norm.fit(data) #to compare 
        return hist,hist_fit,fitted_mean,fitted_std


    
    def convolve_list_arr(self,source,kernel):           
    # convolve lists function, as np supports only arrays convolution         
        source_arr=np.array(source)
        kernel_arr=np.array(kernel)
        result_arr=ndimage.convolve(np.array(source),np.array(kernel), mode='reflect')
        return result_arr


    def convolve_list(self,source,kernel):           
        return list(self.convolve_list_arr(source,kernel))

   
    def rgb_2_gray(self,rgb):
        r, g, b = rgb[:,:,0], rgb[:,:,1], rgb[:,:,2]
        gray = 0.2989 * r + 0.5870 * g + 0.1140 * b
        return gray


    def circle_mask(self,col_n,row_n,col_cent,row_cent, rad):        
      
        mask = [[0 for col_i in range(col_n)] for row_i in range(row_n)]

        for col_i in range(0,col_n):  
            for row_i  in range(0,row_n):
                   
                dist=np.sqrt( (col_i-col_cent)*(col_i-col_cent) + (row_i-row_cent)*(row_i-row_cent) )     
                   
                if dist<rad: 
                    mask[col_i][row_i]=1

        return mask


    def square_mask(self,col_n,col_m,row_n,row_m,col_max,row_max ):


        mask = [[0 for col_i in range(col_max)] for row_i in range(row_max)]

        for col_i in range(col_n,col_m):  
            for row_i  in range(row_n,row_m):
                    mask[col_i][row_i]=1

        return mask

    
    def export_resize_fig_matplotlib(self, plot, f_name, size_x,size_y):
       
        #fig = plt.figure(frameon=False)
        fig=plot.figure
        f_dpi=fig.dpi

        fig.set_size_inches(size_x/f_dpi, size_y/f_dpi)

        ax = plt.Axes(fig, [0., 0., 1., 1.])
        ax.set_axis_off()
        fig.add_axes(ax)
        #ax.imshow(arr)
        fig.savefig(f_name, dpi=f_dpi )
 
        io_buf = io.BytesIO()
        fig.savefig(io_buf, format='raw', dpi=f_dpi)
        io_buf.seek(0)                
        io_buff_arr=np.frombuffer(io_buf.getvalue())

        return io_buff_arr
    

    def merge_png_layers(self, fname_bkg, fname_fg, fname_result): 

        from PIL import Image

        background = Image.open(fname_bkg)
        foreground = Image.open(fname_fg)
        
        foreground.convert('RGBA')
        background.convert('RGBA')
        
        final_2 = Image.new("RGBA", background.size)
        final_2.paste(background, (0,0), background)
        final_2.paste(foreground, (0,0), foreground)
        final_2.save(fname_result)


    def merge_png_layers_transp_r(self, fname_bkg, fname_fg, fname_result): 
        # Merge two png's with transparancy

        from PIL import Image

        bkg_src = Image.open(fname_bkg)
        fg_src = Image.open(fname_fg)
         
        bkg = bkg_src.convert('RGB')
        fg = fg_src.convert('RGB') 
        #r_bkg, g_bkg, b_bkg = np.divide(bkg.split(),256)
        #r_fg, g_fg, b_fg = np.divide(fg.split(),256)
        size_x = np.size(bkg,0)
        size_y = np.size(bkg,1)

        r_bkg_img, g_bkg_img, b_bkg_img = bkg.split()
        r_fg_img, g_fg_img, b_fg_img = fg.split()

              
        r_bkg = np.divide(np.array(r_bkg_img),256)  
        g_bkg = np.divide(np.array(g_bkg_img),256)
        b_bkg = np.divide(np.array(b_bkg_img),256)
        r_fg = np.divide(np.array(r_fg_img),256)
        g_fg = np.divide(np.array(g_fg_img),256) 
        b_fg = np.divide(np.array(b_fg_img),256) 


        r_fg_coeff=np.multiply(r_fg,0.299)
        g_fg_coeff=np.multiply(g_fg,0.587)
        b_fg_coeff=np.multiply(b_fg,0.114)      

        gs_fg=np.array(r_fg_coeff)
        gs_fg=np.add(gs_fg,g_fg_coeff)
        gs_fg=np.add(gs_fg,b_fg_coeff)
          
        r_fin_float=np.multiply(r_bkg,gs_fg)
        r_fin=np.uint8(np.multiply(r_fin_float,255.9999))
        g_fin=np.uint8(np.multiply(g_bkg,255.9999))
        b_fin=np.uint8(np.multiply(b_bkg,255.9999))
        
        rgb_fin = np.zeros((size_x,size_y,3), 'uint8')
        rgb_fin[..., 0] = r_fin
        rgb_fin[..., 1] = g_fin
        rgb_fin[..., 2] = b_fin
        img_fin = Image.fromarray(rgb_fin)

        img_fin.save(fname_result, 'PNG')

        
        
    def merge_two_layers_transp_r_save_png(self, bkg_arr, fg_arr, fname_result): 

        from PIL import Image
        #bkg_arr_max=np.amax(bkg_arr)
        
        size_x = np.size(bkg_arr,0)
        size_y = np.size(bkg_arr,1)

        bkg_arr_max=np.amax(bkg_arr)
        fg_arr_max = np.amax(fg_arr)
        
        bkg_arr_norm=np.divide(bkg_arr,bkg_arr_max) #np.subtract(1,np.divide(bkg_arr,bkg_arr_max)) 

        fg_arr_norm=np.divide(fg_arr,fg_arr_max) 


        fg_arr_r = np.array(fg_arr_norm)
        fg_arr_g = np.subtract(1,fg_arr_norm)
        fg_arr_b = np.subtract(1,fg_arr_norm) 
        
        fin_r_all_px = np.divide(np.add(bkg_arr_norm,fg_arr_r),2)
        fin_g_all_px = np.divide(np.add(bkg_arr_norm,fg_arr_g),2)
        fin_b_all_px = np.divide(np.add(bkg_arr_norm,fg_arr_b),2)

        fin_r = np.array(bkg_arr_norm)
        fin_g = np.array(bkg_arr_norm)
        fin_b = np.array(bkg_arr_norm)
        
        fin_r_mask = (fg_arr_norm>0)*fin_r_all_px
        fin_g_mask = (fg_arr_norm>0)*fin_g_all_px
        fin_b_mask = (fg_arr_norm>0)*fin_b_all_px 
       
        fin_r_bkg = (fg_arr_norm<=0)*bkg_arr_norm
        fin_g_bkg = (fg_arr_norm<=0)*bkg_arr_norm
        fin_b_bkg = (fg_arr_norm<=0)*bkg_arr_norm
        
        fin_r=np.add(fin_r_mask,fin_r_bkg)
        fin_g=np.add(fin_g_mask,fin_g_bkg)
        fin_b=np.add(fin_b_mask,fin_b_bkg)


        fin_r_256=np.uint8(np.multiply(fin_r,255.9999))
        fin_g_256=np.uint8(np.multiply(fin_g,255.9999))
        fin_b_256=np.uint8(np.multiply(fin_b,255.9999))
        
        fin_rgb = np.zeros((size_x,size_y,3), 'uint8')
        
        fin_rgb[..., 0] = fin_r_256
        fin_rgb[..., 1] = fin_g_256
        fin_rgb[..., 2] = fin_b_256

        img_fin = Image.fromarray(fin_rgb)
        img_fin_flip=ImageOps.flip(img_fin)
        img_fin_flip.save(fname_result)

    def merge_three_layers_transp_r_save_png(self, bkg_arr, fg_arr_1, fg_arr_2, fname_result): 

       
        
        size_x = np.size(bkg_arr,0)
        size_y = np.size(bkg_arr,1)

        bkg_arr_max=np.amax(bkg_arr)
        fg_arr_1_max = np.amax(fg_arr_1)
        fg_arr_2_max = np.amax(fg_arr_2)
        
        bkg_arr_norm=np.divide(bkg_arr,bkg_arr_max) #np.subtract(1,np.divide(bkg_arr,bkg_arr_max)) 

        fg_arr_1_norm=np.divide(fg_arr_1,fg_arr_1_max) 
        fg_arr_2_norm=np.divide(fg_arr_2,fg_arr_2_max)

        fg_arr_1_r = np.array(fg_arr_1_norm)
        fg_arr_1_g = np.subtract(1,fg_arr_1_norm)
        fg_arr_1_b = np.subtract(1,fg_arr_1_norm) 

        fg_arr_2_r = np.array(fg_arr_2_norm)
        fg_arr_2_g = np.subtract(1,fg_arr_2_norm)
        fg_arr_2_b = np.subtract(1,fg_arr_2_norm)
                
        fg_arr_r = np.divide(np.add(fg_arr_2_r,fg_arr_1_r),2)
        fg_arr_g = np.divide(np.add(fg_arr_2_g,fg_arr_1_g),2)
        fg_arr_b = np.divide(np.add(fg_arr_2_b,fg_arr_1_b),2)
            
        fin_r_all_px = np.divide(np.add(bkg_arr_norm,fg_arr_r),2)
        fin_g_all_px = np.divide(np.add(bkg_arr_norm,fg_arr_g),2)
        fin_b_all_px = np.divide(np.add(bkg_arr_norm,fg_arr_b),2)

        fin_r = np.array(bkg_arr_norm)
        fin_g = np.array(bkg_arr_norm)
        fin_b = np.array(bkg_arr_norm)

        fin_r_mask = (fg_arr_1_norm>0)*fin_r_all_px
        fin_g_mask = (fg_arr_1_norm>0)*fin_g_all_px
        fin_b_mask = (fg_arr_1_norm>0)*fin_b_all_px 
       
        fin_r_bkg = (fg_arr_1_norm<=0)*bkg_arr_norm
        fin_g_bkg = (fg_arr_1_norm<=0)*bkg_arr_norm
        fin_b_bkg = (fg_arr_1_norm<=0)*bkg_arr_norm
        
        fin_r=np.add(fin_r_mask, fin_r_bkg)
        fin_g=np.add(fin_g_mask ,fin_g_bkg)
        fin_b=np.add(fin_b_mask ,fin_b_bkg)


        fin_r_256=np.uint8(np.multiply(fin_r,255.9999))
        fin_g_256=np.uint8(np.multiply(fin_g,255.9999))
        fin_b_256=np.uint8(np.multiply(fin_b,255.9999))
        
        fin_rgb = np.zeros((size_x,size_y,3), 'uint8')
        
        fin_rgb[..., 0] = fin_r_256
        fin_rgb[..., 1] = fin_g_256
        fin_rgb[..., 2] = fin_b_256

        img_fin = Image.fromarray(fin_rgb)
        img_fin_flip=ImageOps.flip(img_fin)
        img_fin_flip.save(fname_result)


    def save_data_pic(self, data, f_name,size_x, size_y):
    
        fig=plt.figure()
        fig.max_open_warning=40
        fig = plt.figure(figsize=(10,10), dpi=400)
        plt.axis('off')
        plot=plt.imshow(data, cmap='gray')
        fig=plot.figure
        f_dpi=fig.dpi

        fig.set_size_inches(size_x/f_dpi, size_y/f_dpi)
        ax = plt.Axes(fig, [0., 0., 1., 1.])
        ax.set_axis_off()
        fig.add_axes(ax)

        fig.savefig(f_name, dpi=f_dpi )
        fig.clear()
 
        plt.close()
      

    def merge_two_fits(self,fname_1, fname_2, fname_result):

        #LOAD APPLIED MASK FILE (ALREADY RESAMPLED)
        bkg_map=sunpy.map.Map(fname_1)
        fg_map=sunpy.map.Map(fname_2)

        #PLOT THE ORIGINAL DATA INVERTED AND THE APPLIED MASK ON TOP WITH A DIFFERENT COLOR MAP
        fig = plt.figure(figsize=(8,8))
        bkg_map.plot(cmap='Greys')
        #plt.imshow(bkg_map.data, cmap='gray')
        fg_map.data[fg_map.data == 0.] = np.nan
        fg_map.plot(cmap='rainbow')
        fig.savefig(fname_result)
        plt.close()
        

    def adjust_dynrange_data(self, data_arr, base):

        logmax=np.log(data_arr)/np.log(base)
        data_arr_max=np.amax(data_arr)
        data_arr_log=np.divide(np.log(data_arr),np.log(base))
        data_arr_log_norm=np.multiply(data_arr_log,(data_arr_max/logmax))
        return data_arr_log_norm
        
 
    def invert_data_numeric(self, data_arr): 
        
        data_arr_max=np.amax(data_arr)
        data_img_inverted_c=np.subtract(data_arr_max,data_arr)
        return np.asarray(data_img_inverted_c)
        #hint: np.subtract(1,np.divide(bkg_arr,bkg_arr_max))


    def invert_data_img(self, data_arr): 
        
        #data_image=Image.fromarray(data_arr,'L') # try L option         
        data_image=Image.fromarray(data_arr) 
        data_image_RGB=data_image.convert('RGB')
        data_img_inverted = ImageOps.invert(data_image_RGB)
        #data_img_inverted_c=data_img_inverted.convert('L')
        #data_img_inverted_b=ImageOps.invert(data_img_inverted)
        data_img_inverted_c=data_img_inverted.convert('L')
        data_img_inverted_c.save('invert_log.png')
        return np.asarray(data_img_inverted_c)


    def bring_to_int32(self,data_arr):
            
        #int32ii=np.iinfo(np.int32)

        #data_arr_min = np.amin(data_arr)
        #data_arr_plus = np.subtract(data_arr, data_arr_min)
        #data_arr_max = np.amax(data_arr_plus) 
        #data_arr_adj_f = np.round(np.multiply(data_arr_plus,int32ii.max/data_arr_max))
        #return data_arr_adj_f.astype(np.int32)
        int32ii=np.iinfo(np.int32)
        data_arr_max = np.amax(data_arr) 
        data_arr_adj_f = np.round(np.multiply(data_arr,int32ii.max/data_arr_max))
        return data_arr_adj_f.astype(np.int32)

    

    def bring_to_one_float64(self,data_arr):    
    
        data_arr_max = np.amax(data_arr) 
        data_arr_adj_f = np.multiply(data_arr,1/data_arr_max)
        return data_arr_adj_f.astype(np.float64)


    
    def normalize_posterize(self,data, bits):
         print('Normalizing data, bringing to '+str(bits)+' bits resolution...')
         int32ii=np.iinfo(np.int32)
         data_min=np.amin(data) 
         data_plus= np.subtract(data,data_min)
         ratio= np.amax(data_plus)/pow(2,bits) 
         data_div_f = np.divide(data_plus,ratio)
         data_div_f_r = np.round(data_div_f)
         print('Normalizing/bringing the data to [0...'+ str(int32ii.max)+'] scale...'  )
         data_norm_f = np.round(np.multiply(data_div_f_r.astype(np.int32),int32ii.max/pow(2,bits)))
         data_norm = data_norm_f.astype(np.int32)
         return data_norm
    
    
    def set_obj_prop_config_file(self, object,fname): 
           
        file_id = open(fname, 'r')
        lines = file_id.readlines()
      
        for line in lines:
            
            term_1, term_2 = line.split(',')
            
            try: 
               value = float(term_2)
               if (value % 1 == 0):
                   value = int(term_2)

            except ValueError:
                   value = str(term_2)


            if hasattr(object,term_1):
               
                object.__dict__[term_1] = value
            
            else: 

                print("Warning, unable to process config line: " + str(line)) 

        file_id.close()

 
     
  
