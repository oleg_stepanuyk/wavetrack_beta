
class t_preproc_config(object): # preprocessing setup
    
    def __init__(self): 
       
       # Shows if base difference images to to be used.
       self.use_base_diff ='y' 
      
       # Differencing algorithm. Possible values are 'BASE_DIFF' 'RD', 'AWARE'
       
       # 1) 'BASE_DIFF' Base images are created by averaging series of images a few minutes prior
       # to the start of the eruption. Each time steps starts with base difference 
       # images which are constructed by subtracting base images from the current 
       # RAW image of the sequence.
       
       # 2) 'RD' Running difference or persistence running difference images (Ireland et al., 2019) 
       # are used when necessary to determine the moment when the eruption starts, or in case 
       # Wavetrack is used to study an active region or a filament.  Using running difference images 
       # introduces spurious features and is not recommended for discovering the true (projected) 
       # wave shape (Long et al., 2011)

       # 3) 'AWARE' The AWARE algorithm uses more advanced pre-processing  
       # in  persistence  running  difference  images,  to  characterize  the  wave  
       # shapes  along similar flare-centered sectors
       self.diff_algorithm = 'BASE_DIFF' 
       
       # Initial images are reized to x_size y_size specified below 
       self.x_size=1024 
       self.y_size=1024
       
       # Solar disk parameters 
       # Coordinates and center of the solar disk is taken from disk_radius, disk_center_x, disk_center_y
       # in case it is not determined  automatically from FITS file, i.e the parameter 
       # use_fixed_sun_radius='n' is set to 'n' (No)
       self.use_fixed_sun_radius='n' 
       self.disk_radius=400
       self.disk_center_x=500
       self.disk_center_y=500 
  
       # Input data type. 'FITS' or 'raster'
       self.input_data_format='FITS' 
       # Input data level. Could be 'AIA_1.5', 'AIA_1', AIA_1_resized_log (experimental)
       self.data_lev='AIA_1.5' 
       
       # Threshold for absolute values while initial file read
       self.low_threshold=-10000
       self.high_threshold=40000
       # Threshold for base difference files
       self.low_threshold_bd= -100
       self.high_threshold_bd= 70

       # Invert data during initial file read 
       self.data_invert_img_load='n'
       # Invert data within (t_pipeline class) 
       self.data_invert_pipeline='y'
      
       # Use PIL library or numeric for image inversion for main image and second, 
       # background "thread" which is used for visual interpretation of the results 
       self.data_invert_method='img' 
       self.data_invert_method_bkg='img'

       # Log adjustment of Dynamic Range
       self.adjust_dynrange='n'
       self.dynrange_logbase=20
       
       # Load original data as background 
       self.bkg_load_original='y'
       # Invert background images 
       self.data_invert_bkg='y'

       # Image resize/rebin parameters
       self.resize='y'
       # Use manual rebin patch to adjust header metadata ? 
       self.manual_rebin_patch='n'
       # Use sunpy resample instead of manual fits/raster resize
       self.sunpy_resample='n'
       # Rebin factor. To be used together with rebin patch
       self.rebin_factor=4
       
       # Input data with consequitive timesteps in FITS/raster format
       self.data_path='/data/'
       # FITS/raster data to create average base image. Usually 3-5 files 
       # a 2-5 minutes prior to the event 
       self.base_data_path='/base_data/'

       # Normalize data after Base/Running/Persistent(AWARE) differencing 
       self.normalize_init_data='y'
       # Circle mask to replace solar disk with zero pixels  
       self.apply_circle_mask='n'
       
       # Square mask to focus on a certain area of the image, ignoring the rest of the pixels
       # Useful when the region where the event takes place is known in advance
       self.x_1_sq = 530
       self.x_n_sq = 999
       self.y_1_sq = 540
       self.y_n_sq = 790

       # Internal/debugging parameters, not to be modified 
       self.rsun_coeff = 1.00

class t_decompose_config(object): # decomposition setup

    
    def __init__(self):
        
        # An image bit depth determines the number of intensity levels to which an image could be 
        # theoretically resolved. Posterisation is the conversion of a continuous gradation of tone 
        # to several regions of fewer tones and happens naturally when image bit depth has been decreased.
       
        self.resolution_bits = 24

        # A-Trous Wavelet parameters. Amount of scales and weight coefficients to be used for image 
        # reconstruction 
        self.scales_n_max = 5
        self.scales_weights=[0,0,0,1,1,0,0,0,0,0]
       
        # Parameters for post-processing wavelet coefficients 
        
        # Additional PILLOW library parameters for PIL normalize and median filter 
        self.normalize='n'
        self.PIL_posterize='n'
        self.median_filt='n'
        self.median_scale = 3
        self.PIL_posterize_ratio = 3

        # Parameters for feature recognition setup 
        # Gradient method 
        # The gradient field of the image is the result of applying the gradient operator to an image. 
        # In this case, the gradient field of the final object image (after the object mask is applied 
        # to the original image) is used for visual interpretation but can also serve as additional criteria 
        # for more robust object segmentation, as it highlights object contours, allows to determine saddle point
        # locations, and to calculate gradient vectors. Image gradients can also be used as part of training sets
        # for deep convolutional networks. By default, we use the Sobel-Feldman operator, a discrete differentiation 
        # operator, to compute an approximation of the gradient of the image intensity function.  At each pixel of 
        # the image, the result would be either the corresponding gradient vector, or the norm of this vector. 
        #  
        # kernel_sobel_gx= [[1,0,-1],
        #                  [2,0,-2],
        #                  [1,0,-1]]

        # kernel_sobel_gy= [[1,2,1],
        #                  [0,0,0],
        #                  [-1,-2,-1]]
   
        # Alternatively, a simple gradient could be applied (grad_method=Simple_grad ) 
        # self.kernel_grad = [[1,0,-1],
        #                    [0,0,0],
        #                    [-1,0,1]], 
        #
        # or curvature (grad_method=Hagenaer in each pixel point could be calculated according to 
        # Hagenaer et. al. 1999 Astr.J. 511:932-944,1999   
         
    
        self.grad_method='Sobel-Feldman'

        # Relative thresholding (statistic) 
        # Due to the significantly different statistical distributions
        # of pixel intensities, on-disk and off-limb parts of images oftenly are 
        # pre-processed separately (apply_thrhold_circle_mask = 'y'),
        # and the results are merged afterwards. 
        self.apply_thrhold_circle_mask = 'y'    

        # Threshold parameters for the limb (outside_thrhold_sigma), disk (thrhold_sigma) and 
        # gradient field (grad_thrhold_sigma). Multiplicative constant before standard deviations count
        # (i.e roughly, "amount" of standard deviations)
        self.thrhold_sigma = 2.7
        self.grad_thrhold_sigma = 2.7
        self.outside_thrhold_sigma = 5.3
        
        # Minimum structure size in pixels. Smaller structures would be ommited 
        self.min_struct_size=400
        
        # Check which structures overlap within neighboring timesteps 
        self.search_overlap='y'
        
        # Split into intensity levels manually ( usually right setup of absolute thresholding )
        self.split_intensities = 'n'
        
        # Since each of the wavelet decomposition levels effectively represents its own 
        # image detail level separately, one of the higher decomposition levels (in practice fifth or sixth)
        # would reveal shapes of relatively large objects, almost completely omitting small scale
        # details and noise. After proper thresholding a binarised mask of a high decomposition scale is
        # applied to the joint structures mask as an additional filter. 
        # One of the higher levels of wavelet decomposition of the solar image is taken 
        # as a "reference mask", limiting the area where objects of interest are located 
        # by the shape of that mask.     
        self.reference_scale_n = 4  
        # Use coefficient ( or scale otherwise)
        self.coeff_as_reference ='y'
        

        # Segmentation of the image is done by iteratively splitting it into quadrants while each 
        # object is renumbered in accordance with the quadrant number it belongs to at each iteration, 
        # provided the object does not cross the border of the quadrant. 
        # The value of the corresponding gradient field (and thus, a possible contour location) serves
        # as an additional criterion for object selection. Initial estimation of the objects count 
        # and quadrant locations is done via a Monte-Carlo algorithm. Alternatively, one-pass ABC mask 
        # could be used (experimental, not recommended) 
        self.use_abc_mask = 'n' 
        
        # Verbose output mode. Save all intermidiate steps, decompositions and structures output_log folder
        # used mainly for testing purposes 
        self.verbose_output = 'y'
        self.save_scales = 'y'
        
        # Background image is a parallel "thread" with separate pre- post- and decomposition parameters 
        # Used solely for visual interpretation, ./visualize folder
        self.bkg_resolution_bits = 24
        self.bkg_thrhold_sigma=0.5
        self.bkg_grad_thrhold_sigma= 0.5
        self.bkg_outside_thrhold_sigma=0.5
        self.bkg_scales_weights=[0,1,1,1,1,1,1,1,0,0]
    

        self.struct_save_path = '/output_structs/'
        self.objects_mla_save_path = '/output_objects_mla/'
        self.log_save_path = '/output_log/'
        self.grad_save_path = '/output_gradients/'
        self.results_coord_fname = '/output_structs/strucures_coord.txt'
        self.results_overlap_fname = '/output_structs/strucures_overlap.txt'
        self.trainset_save_path = '/trainset/'
        self.visualize_save_path = '/visualize/'
        
        # Internal/debugging parameters. Not to be modified 
        self.override_w_bkg = 'n'
        self.override_bkg_w_orig = 'n'
        self.use_scales_not_coeff='n'
        self.optimal_w_coeff = 3
        self.objects_gradients = 'n'
        self.use_imsave='y'
        self.dist_per_pixel=1
